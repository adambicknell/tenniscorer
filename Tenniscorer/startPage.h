//
//  ViewController.h
//  Tenniscorer
//
//  Created by Adam on 09/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface startPage : UIViewController {
    
    IBOutlet UIButton *btnStart;
    IBOutlet UIButton *btnHistory;
    IBOutlet UIButton *btnSettings;
    IBOutlet UIButton *btnExit;
    
}

@end
