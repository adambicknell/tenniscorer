//
//  startSingles.m
//  Tenniscorer
//
//  Created by Adam on 10/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import "startSingles.h"

@interface startSingles ()

@end

@implementation startSingles

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
