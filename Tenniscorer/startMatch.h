//
//  startMatch.h
//  Tenniscorer
//
//  Created by Adam on 10/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface startMatch : UIViewController {
   
    IBOutlet UIButton *startSingles;
    IBOutlet UIButton *startDoubles;
}

@end
