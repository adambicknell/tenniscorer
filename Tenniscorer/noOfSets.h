//
//  noOfSets.h
//  Tenniscorer
//
//  Created by Adam on 10/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface noOfSets : UIViewController {
    
    IBOutlet UIButton *oneSet;
    IBOutlet UIButton *threeSets;
    IBOutlet UIButton *fiveSets;
    
}

@end
